El caso de uso/flujo a implementar es el siguiente:

1. Buscar película

2. Mostrar resultados

3. Ver detalle de película seleccionada: mostrar imágenes, estrellas, descripción de la película 

y trailer (entregados por la api de imdb)

4. Agregar película a favoritos (marcar con corazón del diseño)

5. Agregar película a lista de reproducción existente o nueva (botón + del diseño)

6. Ver en sitio de imdb (botón flecha superior derecha del diseño)

:)


##Stack

- AngularJS
- MaterializeCSS
- FontAwesome